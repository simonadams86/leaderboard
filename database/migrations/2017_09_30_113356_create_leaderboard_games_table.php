<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Leaderboard\LeaderboardGame;

class CreateLeaderboardGamesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('club_leaderboard_game');
        Schema::dropIfExists('leaderboard_games');

        Schema::create('leaderboard_games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('display_name');
            $table->timestamps();
        });

        $this->insertGames();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaderboard_games');
    }

    private function insertGames()
    {
        LeaderboardGame::firstOrCreate([
            'name' => 'fifa17',
            'display_name' => 'Fifa 17',
        ]);

        LeaderboardGame::firstOrCreate([
            'name' => 'fifa18',
            'display_name' => 'Fifa 18',
        ]);
    }
}
