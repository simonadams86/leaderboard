<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGamesTableAddHomeClubIdAwayClubId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {

            $table->unsignedInteger('home_club_id')->nullable()->after('type');
            $table->unsignedInteger('away_club_id')->nullable()->after('home_club_id');

            $table->foreign('home_club_id')
                ->references('id')
                ->on('clubs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('away_club_id')
                ->references('id')
                ->on('clubs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropForeign(['home_club_id']);
            $table->dropForeign(['away_club_id']);
            $table->dropColumn(['home_club_id','away_club_id']);
        });
    }
}
