<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Leaderboard\Model\Game;

class AlterGamesDropTypeAddLeaderboardGameId extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {

            $table->dropColumn(['type']);

            $table->unsignedInteger('leaderboard_game_id')
                ->nullable()
                ->after('period');

            $table->foreign('leaderboard_game_id')
                ->references('id')
                ->on('leaderboard_games')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        $this->setLeaderboardGameId();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {

            $table->string('type')
                ->default('Fifa')->after('period');

            $table->dropForeign(['leaderboard_game_id']);
            $table->dropColumn(['leaderboard_game_id']);
        });
    }

    private function setLeaderboardGameId()
    {
        $games = \Leaderboard\Model\Game::all();

        $receivedFifa18 = \Carbon\Carbon::createFromFormat('Y-m-d', '2017-09-29')->startOfDay();

        $games->each(function (Game $game) use ($receivedFifa18) {
            $game->leaderboard_game_id = $game->created_at > $receivedFifa18 ? 2 : 1;
            $game->save();
        });
    }
}
