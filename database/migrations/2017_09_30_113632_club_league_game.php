<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Leaderboard\Club;

class ClubLeagueGame extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('club_leaderboard_game');

        Schema::create('club_leaderboard_game', function (Blueprint $table) {

            $table->unsignedInteger('club_id');
            $table->unsignedInteger('leaderboard_game_id');
            /*Only foreign key!*/
            $table->unsignedInteger('league_id');

            $table->integer('att')
                ->nullable();
            $table->integer('mid')
                ->nullable();
            $table->integer('def')
                ->nullable();
            $table->integer('ovr')
                ->nullable();

            $table->foreign('club_id')
                ->references('id')
                ->on('clubs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('leaderboard_game_id')
                ->references('id')
                ->on('leaderboard_games')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('league_id')
                ->references('id')
                ->on('leagues')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['club_id', 'leaderboard_game_id']);
        });

        $this->createClubsWithRelations();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_leaderboard_game');
    }

    private function createClubsWithRelations()
    {
        $clubs = Club::all()->keyBy('id');
        $clubs->each(function (Club $club) {
            $club->leaderBoardGame()->attach(
                1,
                [
                    'league_id' => $club->league_id,
                    'att' => $club->att,
                    'mid' => $club->mid,
                    'def' => $club->def,
                    'ovr' => $club->ovr,
                ]
            );
        });

        $filename = 'app/fifa18-2.csv';
        $path = storage_path($filename);

        $csv = League\Csv\Reader::createFromPath($path);

        $result = $csv->fetchAll();

        $needle = 'League:';

        $leagueId = null;

        foreach($result as $item) {

            $data = array_first($item);

            if (!$data) continue;

            if (str_contains($data, $needle)) {
                $leagueId = explode($needle, $data)[ 1 ];
                continue;
            }

            /** @var Club $clubId */
            $clubName = str_replace(' *', '', $data);

            //find id with search method
            $clubsContaining = $clubs->filter(function (Club $club, $key) use ($clubName) {
                return str_contains($club->name, $clubName);
            });

            if ($clubsContaining->count()) {
                $moreClubs = $clubsContaining->filter(function (Club $club, $key) use ($clubName) {
                    return $club->name == $clubName;
                });

                if (!$moreClubs->count()) {
                    $club = $clubsContaining->first();
                } else {
                    $club = $moreClubs->first();
                }
            } else {
                $club = $clubsContaining->first();
            }

            if ($club) {
                $clubs->forget($club->id);
            }

            if (!$club) {
                $club = Club::create([
                    'name' => $clubName,
                    'league_id' => $leagueId,
                    'att' => 0,
                    'mid' => 0,
                    'def' => 0,
                    'ovr' => 0,
                ]);
            }

            $club->leaderBoardGame()->attach(
                2,
                [
                    'league_id' => $club->league_id,
                    'att' => $club->att,
                    'mid' => $club->mid,
                    'def' => $club->def,
                    'ovr' => $club->ovr,
                ]
            );
        }
    }
}
