<?php

use Faker\Generator as Faker;

$factory->define(\Leaderboard\Model\Game::class, function (Faker $faker) {

    $homeGoals = $faker->numberBetween(0, 4);
    $awayGoals = $faker->numberBetween(0, 4);

    $result = "{$homeGoals}-{$awayGoals}";

    return [
        'result' => $result,
        'period' => \Carbon\Carbon::now()->toDateString(),
    ];
});
