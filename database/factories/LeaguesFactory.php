<?php

use Faker\Generator as Faker;

$factory->define(\Leaderboard\League::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
