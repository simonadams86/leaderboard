<?php

use Faker\Generator as Faker;

$factory->define(\Leaderboard\Club::class, function (Faker $faker) {

    $leagueId = 1;

    return [
        'name' => $faker->name,
        'league_id' => $leagueId,
        'att' => $faker->numberBetween(60, 100),
        'mid' => $faker->numberBetween(60, 100),
        'def' => $faker->numberBetween(60, 100),
        'ovr' => $faker->numberBetween(60, 100),
    ];
});
