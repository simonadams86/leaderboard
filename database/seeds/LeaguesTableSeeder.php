<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class LeaguesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $directory = storage_path('app/teams');

        $files = File::allFiles($directory);


        foreach($files as $file) {
            $needle = ' - Sheet1';

            $filename = $file->getFilename();

            if (str_contains($filename, $needle)) {
                $name = explode($needle, $filename)[ 0 ];

                factory(\Leaderboard\League::class)->create(['name' => $name]);
            }
        }
    }
}
