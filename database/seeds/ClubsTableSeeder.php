<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use League\Csv\Reader;

class ClubsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createClubsWithStats();
    }

    /**
     * @deprecated
     */
    private function createClubsWithStats()
    {
        $directory = storage_path('app/teams');

        $files = File::allFiles($directory);

        foreach($files as $file) {

            $filename = $file->getFilename();

            $needle = ' - Sheet1';

            if (str_contains($filename, $needle)) {

                $leagueName = explode($needle, $filename)[ 0 ];

                $leagueId = \Leaderboard\League::where('name', $leagueName)->first()->id;

                $csv = Reader::createFromPath($directory.'/'.$filename);
                $headers = array_map('strtolower', $csv->fetchOne());

                $result = $csv->setOffset(1)->fetchAll();

                $models = [];

                foreach($result as $item) {
                    $models[] = array_combine($headers, $item);
                }

                foreach($models as $key => $club) {

                    $club = array_filter($club, function ($value) {
                        return $value !== '';
                    });

                    $club[ 'league_id' ] = $leagueId;
                    unset($club[ 'league' ]);

                    factory(\Leaderboard\Club::class)->create($club);
                }
            }
        }
    }
}
