<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [
            [
                'result' => '0-0',
                'period' => '2017-09-01',
            ],
            [
                'result' => '1-0',
                'period' => '2017-09-01',
            ],
            [
                'result' => '2-0',
                'period' => '2017-09-01',
            ],
            [
                'result' => '1-0',
                'period' => '2017-09-01',
            ],
            [
                'result' => '0-1',
                'period' => '2017-09-01',
            ],
            [
                'result' => '1-0',
                'period' => '2017-09-01',
            ],

        ];

        foreach($games as $game) {
            $tempGame = factory(\Leaderboard\Model\Game::class)->create($game);

            $tempGame->users()->attach(1, ['is_home' => true]);
            $tempGame->users()->attach(2, ['is_home' => true]);
            $tempGame->users()->attach(4, ['is_home' => false]);
            $tempGame->users()->attach(7, ['is_home' => false]);
        }
    }
}
