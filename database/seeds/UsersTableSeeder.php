<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Troels Johannesen',
                'email' => 'troels@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Simon Adams',
                'email' => 'simon@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Mads Wolf Lundholm',
                'email' => 'mads@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Christian Dolberg Rasmussen',
                'email' => 'christian@wecode.dk',
                'nickname' => 'The Corruptor',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Alexander Lindkjær',
                'email' => 'alexander@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Cecilie Frode-Jensen',
                'email' => 'cecilie@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'René Dyja',
                'email' => 'rene@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
            [
                'name' => 'Jonas Hoffmann',
                'email' => 'jonas@wecode.dk',
                'slack_webhook_url' => 'https://hooks.slack.com/services/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',
                'password' => bcrypt('troels0714'),
            ],
        ];

        foreach($users as $user) {
            factory(\Leaderboard\Model\User::class)->create($user);
        }
    }
}
