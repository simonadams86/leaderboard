<?php

namespace Leaderboard\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class LeagueTable
{

    /** @var  Collection $teams */
    private $teams;

    public function __construct()
    {
        $this->teams = new Collection();
    }

    public function addTeam(Statistics $team)
    {
        $this->teams->push($team);
    }

    public function generateTable(Request $request)
    {
        $this->setPositions();

        $this->applyFilters($request);

        return $this->paginated($request);
    }

    public function setPositions()
    {
        /** @var Statistics $teamMostGoals */
        $teamMostGoals = $this->teams
            ->sortByDesc(
                function (Statistics $statistics) {
                    return $statistics->goalsPerGame();
                }
            )->first();

        $teamMostGoals->setMostGoalsPerGame($teamMostGoals->goalsPerGame() > 0 && true);

        $this->teams = $this->teams
            ->sortByDesc(
                function (Statistics $statistics) {
                    return $statistics->pointsPerGame();
                }
            )->values()
            ->filter(function (Statistics $statistics, $key) {
                return $statistics->totalGames() !== 0 ||
                       ($statistics->metaData[ 'isLastMonthsWinner' ] || $statistics->metaData[ 'isLastMonthsLoser' ]);
            })
            ->values()
            ->each(
                function (Statistics $statistics, $key) {
                    $leaguePosition = $key + 1;
                    $statistics->setLeaguePosition($leaguePosition);
                }
            );

        return $this;
    }

    /**
     * @param Request $request
     */
    private function applyFilters(Request $request)
    {
        $sort = explode('|', $request->sort);

        if ($key = $sort[ 0 ]) {
            $this->teams = $this->teams->sortBy(function ($stats) use ($sort, $key) {
                return $stats->$key();
            }, SORT_REGULAR, $sort[ 1 ] !== 'desc')->values();
        }

        if ($filter = $request->filter) {
            $this->teams = $this->teams->filter(function ($statistics) use ($filter) {
                return str_contains(strtolower($statistics->name()), strtolower($filter));
            })->values();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function paginated(Request $request)
    {
        return $this->teams->paginate($request->perPage ? : 15, $request->page, $options = []);
    }

    /**
     * @return Collection
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }
}