<?php

namespace Leaderboard\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Leaderboard\Club;
use Leaderboard\LeaderboardGame;

class Game extends Model
{

    use Notifiable, SoftDeletes;

    protected $guarded = [];

    protected $appends = ['score', 'score_short', 'summary', 'period_formatted', 'img_path'];

    const HOME = 'home';
    const TIE = 'tie';
    const AWAY = 'away';
    const WINNERS = 'winners';
    const LOSERS = 'losers';

    protected $slack_webhook_url;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->slack_webhook_url = config('slack.public_channel_webhook_url');
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function (Game $game) {
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withPivot(['is_home']);
    }

    public function homeClub()
    {
        return $this->belongsTo(Club::class, 'home_club_id');
    }

    public function awayClub()
    {
        return $this->belongsTo(Club::class, 'away_club_id');
    }

    public function leaderboardGame()
    {
        return $this->belongsTo(LeaderboardGame::class);
    }

    public function getImgPathAttribute()
    {
        $name = $this->leaderboardGame()->value('name');
        return "/images/{$name}.png";
    }

    /**
     * @param int|User $user
     * @param bool $isHome
     */
    public function addUser($user, $isHome = false)
    {
        $this->users()->attach($user, ['is_home' => $isHome]);
    }

    /**
     * @return Collection
     */
    public function homeTeam()
    {
        return $this->users->filter(function ($user) {
            return $user->pivot->is_home;
        });
    }

    /**
     * @return Collection
     */
    public function awayTeam()
    {
        return $this->users->filter(function ($user) {
            return !$user->pivot->is_home;
        });
    }

    /**
     * @return Collection
     */
    public function teams()
    {
        return new Collection([
            self::HOME => $this->homeTeam(),
            self::AWAY => $this->awayTeam(),
        ]);
    }

    /**
     * @return string
     */
    public function summary()
    {
        $glue = ' og ';
        $key = 'game_name';

        if (!$this->winners()->count() || !$this->losers()->count()) {
            $home = $this->homeTeam()->implode($key, $glue);
            $away = $this->awayTeam()->implode($key, $glue);
            return "{$home} spillede uafgjort {$this->result} med {$away}";
        }

        $winners = $this->winners()->implode($key, $glue);

        $losers = $this->losers()->implode($key, $glue);

        return "{$winners} slog {$losers} med {$this->resultFormatted()}";
    }

    public function score($shortForm = false)
    {
        if ($shortForm) {
            $key = 'initials';
        } else {
            $key = 'game_name';
        }
        $glue = ', ';

        return "{$this->homeScore($key, $glue)}-{$this->awayScore($key, $glue)}";
    }

    /**
     * @param $key
     * @param $glue
     * @return string
     */
    private function homeScore($key, $glue): string
    {
        $homeTeam = $this->homeTeam()->implode($key, $glue);
        $result = $this->resultsArray();
        $homeScore = $result[ 0 ];
        return "{$homeTeam} {$homeScore}";
    }

    /**
     * @param $key
     * @param $glue
     * @return string
     */
    private function awayScore($key, $glue): string
    {
        $awayTeam = $this->awayTeam()->implode($key, $glue);
        $result = $this->resultsArray();
        $awayScore = $result[ 1 ];
        return "{$awayScore} {$awayTeam}";
    }

    public function getGoalsFor(User $user)
    {
        return $this->homeTeam()->contains($user)
            ? $this->resultsArray()[ 0 ]
            : $this->resultsArray()[ 1 ];
    }

    public function getGoalsAgainst(User $user)
    {
        return $this->homeTeam()->contains($user)
            ? $this->resultsArray()[ 1 ]
            : $this->resultsArray()[ 0 ];
    }

    /**
     * @return Collection
     */
    public function winners()
    {
        return $this->decide()
            ->get(self::WINNERS);
    }

    /**
     * @return Collection
     */
    public function losers()
    {
        return $this->decide()
            ->get(self::LOSERS);
    }

    /**
     * @return array
     */
    public function resultsArray()
    {
        return explode('-', $this->result, 2);
    }

    /**
     * @return Collection
     */
    public function decide()
    {
        $result = $this->resultsArray();

        if ($result[ 0 ] > $result[ 1 ]) {
            return new Collection([
                self::WINNERS => $this->homeTeam(),
                self::LOSERS => $this->awayTeam(),
            ]);
        } elseif ($result[ 0 ] === $result[ 1 ]) {
            return new Collection([
                self::WINNERS => new Collection(),
                self::LOSERS => new Collection(),
            ]);
        } else {
            return new Collection([
                self::WINNERS => $this->awayTeam(),
                self::LOSERS => $this->homeTeam(),
            ]);
        }
    }

    public function getScoreAttribute()
    {
        return $this->score();
    }

    public function getScoreShortAttribute()
    {
        return $this->score($shortForm = true);
    }

    public function getSummaryAttribute()
    {
        return $this->summary();
    }

    public function getPeriodFormattedAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->period)->format('l d-m-y');
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return $this->slack_webhook_url;
    }

    /**
     * Reverse home and away to display win correctly for summary
     * @return mixed|string
     */
    private function resultFormatted()
    {
        if (!$this->awayTeam()->diff($this->winners())->count()) {
            $resultsArray = $this->resultsArray();
            $formattedResult = $resultsArray[ 1 ].'-'.$resultsArray[ 0 ];
        } else {
            $formattedResult = $this->result;
        }
        return $formattedResult;
    }
}
