<?php

namespace Leaderboard\Model;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements Jsonable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['game_name', 'initials'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function games()
    {
        return $this->belongsToMany(Game::class)
            ->withPivot(['is_home']);
    }

    /**
     * @param int|Game $game
     * @param bool $isHome
     */
    public function joinGame($game, $isHome = false)
    {
        $this->games()->attach($game, ['is_home' => $isHome]);
    }

    /**
     * @param Carbon|null|string $period
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getResults($period = null)
    {
        if (!$period) $period = '%';

        $period = $this->getPeriodFormat($period);

        return $this->games()->where('period', 'LIKE', $period)->get();
    }

    /**
     * @param Carbon|null $period
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getResultsInPeriod(Carbon $period = null)
    {
        if (!$period) $period = Carbon::now();

        $period = $this->getPeriodFormat($period);

        return $this->getResults($period);
    }

    /**
     * @return string
     */
    public function getGameNameAttribute()
    {
        if ($this->nickname) return $this->nickname;

        return array_first(explode(' ', $this->name));
    }

    public function getInitialsAttribute()
    {
        $initials = '';

        foreach(explode(' ', $this->name) as $word) {
            $initials .= mb_substr($word, 0, 1, 'utf-8');
        }

        return strtoupper($initials);
    }

    /**
     * @param Carbon|string $period
     * @return string
     */
    private function getPeriodFormat($period): string
    {
        if (!$period instanceof Carbon) return $period;

        return $period->format('Y-m%');
    }
}
