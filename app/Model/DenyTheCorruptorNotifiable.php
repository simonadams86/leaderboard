<?php
/**
 * Created by PhpStorm.
 * User: simonadams
 * Date: 03/09/2017
 * Time: 17.34
 */

namespace Leaderboard\Model;

use Illuminate\Notifications\Notifiable;

class DenyTheCorruptorNotifiable
{

    use Notifiable;

    protected $slack_webhook_url;

    public function __construct()
    {
        $this->slack_webhook_url = config('slack.public_channel_webhook_url');
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return $this->slack_webhook_url;
    }
}