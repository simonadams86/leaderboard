<?php
/**
 * Created by PhpStorm.
 * User: simonadams
 * Date: 02/09/2017
 * Time: 18.15
 */

namespace leaderboard\Model;

use Illuminate\Database\Eloquent\Collection;

class Table
{

    protected $statistics;

    /**
     * Table constructor.
     * @param Collection $statistics
     */
    public function __construct(Collection $statistics)
    {
        $this->statistics = $statistics;
    }

    public function make()
    {
        return $this->statistics;
    }
}