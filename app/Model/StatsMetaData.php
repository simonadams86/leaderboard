<?php

namespace Leaderboard\Model;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

class StatsMetaData implements Arrayable
{

    protected $games;

    protected $isUndefeated = false;

    protected $isLastMonthsWinner = false;

    protected $isLastMonthsLoser = false;

    protected $hasNoWins = false;

    protected $form;

    /**
     * @var User
     */
    private $user;

    public function __construct(Collection $games, User $user)
    {
        $this->games = $games;
        $this->user = $user;

        $this->isUndefeated();
        $this->hasNoWins();
        $this->isLastMonthsWinner();
        $this->isLastMonthsLoser();
        $this->setForm();
    }

    public function isUndefeated()
    {
        if ($this->games->count() < 5) {
            return $this->isUndefeated = false;
        }

        foreach($this->games as $game) {

            if ($game->losers()->contains($this->user)) {
                $this->isUndefeated = false;
                break;
            }
            $this->isUndefeated = true;
        }
    }

    public function hasNoWins()
    {
        if ($this->games->count() < 5) {
            return $this->hasNoWins = false;
        }

        foreach($this->games as $game) {

            if ($game->winners()->contains($this->user)) {
                $this->hasNoWins = false;
                break;
            }
            $this->hasNoWins = true;
        }
    }

    /**
     * Important for sending json collection of object!
     * @return string
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * TODO
     * @return bool
     */
    public function isLastMonthsWinner()
    {
        $this->isLastMonthsWinner = $this->user->id == 1;
    }

    public function setForm()
    {
        $this->form = [];

        foreach($this->games as $game) {
            if ($game->losers()->contains($this->user)) {
                $this->form [] = 'L';
            } elseif ($game->winners()->contains($this->user)) {
                $this->form [] = 'W';
            } else {
                $this->form [] = 'D';
            }
        }

        $this->form;
    }

    /**
     * TODO
     * @return bool
     */
    public function isLastMonthsLoser()
    {
        $this->isLastMonthsLoser = $this->user->id == 7;
    }
}
