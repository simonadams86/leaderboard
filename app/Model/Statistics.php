<?php
/**
 * Created by PhpStorm.
 * User: simonadams
 * Date: 02/09/2017
 * Time: 18.13
 */

namespace Leaderboard\Model;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Notifications\Notifiable;

class Statistics implements Arrayable
{

    use Notifiable;

    protected $id;

    protected $user;

    protected $name;

    protected $games;

    protected $latestGames;

    protected $results;

    protected $totalGames;

    protected $wins = 0;

    protected $draws = 0;

    protected $losses = 0;

    protected $goalsFor = 0;

    protected $goalsAgainst = 0;

    protected $winPercentage = 0.00;

    protected $points = 0;

    protected $pointsPerGame = 0.0000;

    protected $goalsPerGame = 0.0000;

    protected $mostGoalsPerGame = false;

    protected $fields;

    protected $leaguePosition = 0;

    protected $form;

    protected $latestGamesAllTime;

    /** @var array $metaData */
    public $metaData;

    public function __construct(User $user, Carbon $period = null)
    {
        $this->id = $user->id;

        $this->user = $user;

        $this->name = $user->game_name;

        $this->games = $user->getResults($period);

        $this->latestGamesAllTime = $user->getResults()->sortBy('created_at')->take(5)->values();

        $this->latestGames = $this->games->sortByDesc('created_at')->take(5)->values();

        $this->totalGames = $this->games->count();

        /*TODO move up*/
        $this->metaData = (new StatsMetaData($this->latestGames, $this->user))->toArray();

        $this->init();
    }

    public function totalGames()
    {
        return $this->totalGames;
    }

    public function name()
    {
        return $this->name;
    }

    public function user()
    {
        return $this->user;
    }

    public function wins()
    {
        return $this->wins;
    }

    public function losses()
    {
        return $this->losses;
    }

    public function draws()
    {
        return $this->draws;
    }

    public function goalsFor()
    {
        return $this->goalsFor;
    }

    public function goalsAgainst()
    {
        return $this->goalsAgainst;
    }

    public function winPercentage()
    {
        return $this->winPercentage;
    }

    public function points()
    {
        return $this->points;
    }

    public function pointsPerGame()
    {
        return $this->pointsPerGame;
    }

    private function init()
    {
        $this->games->each(function (Game $game) {

            $this->setResults($game);
            $this->setScore($game);
        });
        $this->setWinPercentage();
        $this->setPointsPerGame();
        $this->setGoalsPerGame();

        /** @var Game $game */
        $game = $this->latestGames->first();
        $lastResult = $game ? $this->resultString($game) : 'Ingen kampe spillet';

        $this->fields = $this->fields($lastResult);
    }

    /**
     * @param Game $game
     */
    private function setResults(Game $game)
    {
        if ($game->winners()->contains($this->user)) {
            $this->wins++;
            $this->points += 3;
        } elseif ($game->losers()->contains($this->user)) {
            $this->losses++;
        } else {
            $this->draws++;
            $this->points += 1;
        }
    }

    private function setScore(Game $game)
    {
        $this->goalsFor += $game->getGoalsFor($this->user);
        $this->goalsAgainst += $game->getGoalsAgainst($this->user);
    }

    private function setWinPercentage()
    {
        if ($this->wins) {

            $pct = $this->wins / $this->games->count() * 100;

            $this->winPercentage = number_format(
                $pct, '2', ',', '.'
            );
        }
    }

    private function setPointsPerGame()
    {
        if ($this->points) {

            $ppg = $this->points / $this->games->count();

            $this->pointsPerGame = number_format(
                $ppg, '4'
            );
        }
    }

    private function setGoalsPerGame()
    {
        if ($this->goalsFor) {

            $gpg = $this->goalsFor / $this->games->count();

            $this->goalsPerGame = number_format(
                $gpg, '4'
            );
        }
    }

    /**
     * Important for sending json collection of object!
     * @return string
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return $this->user->slack_webhook_url;
    }

    public function routeNotificationForSmsEagle()
    {
        return $this->user->phone;
    }

    /**
     * @param null|int $position
     * @return array
     */
    public function dailySlackMessage($position = null)
    {
        $message = [];

        $message[ 'title' ] = "Hej {$this->name}.";

        if (intval($position)) {
            $message[ 'title' ] .= " Du ligger lige nu nr. {$position}";
        }

        /*TODO Add profile later*/
        $message[ 'link_title' ] = 'Se Leaderboard';
        $message[ 'url' ] = config('app.url');

        $message[ 'summary' ] = 'Månedens status';

        $message[ 'fields' ] = $this->fields;

        return $message;
    }

    public function dailySmsMessage($position = null)
    {
        $message = "Hej {$this->name}.";

        if (intval($position)) {
            $message .= " Du ligger lige nu nr. {$position}";
        }

        return $message;
    }

    /**
     * @param $game
     * @return string
     */
    private function resultString(Game $game): string
    {
        $goalsFor = $game->getGoalsFor($this->user);
        $goalsAgainst = $game->getGoalsAgainst($this->user);

        if ($game->winners()->contains($this->user)) {
            $lastResult = "Blev vundet {$goalsFor}-{$goalsAgainst}";
        } elseif ($game->losers()->contains($this->user)) {
            $lastResult = "Blev tabt {$goalsFor}-{$goalsAgainst}";
        } else {
            $lastResult = "Blev spillet uafgjort {$goalsFor}-{$goalsAgainst}";
        }
        return $lastResult;
    }

    /**
     * @param $lastResult
     * @return array
     */
    private function fields($lastResult): array
    {
        return [
            'Kampe' => $this->totalGames,
            'Seneste resultat' => $lastResult,
            'Vundet' => $this->wins,
            'Uafjort' => $this->draws,
            'Tabt' => $this->losses,
            'PPK' => $this->pointsPerGame,
        ];
    }

    /**
     * @return int
     */
    public function getLeaguePosition(): int
    {
        return $this->leaguePosition;
    }

    /**
     * @param int $leaguePosition
     */
    public function setLeaguePosition(int $leaguePosition)
    {
        $this->leaguePosition = $leaguePosition;
    }

    public function setGoalsPerGamePos($gpg)
    {
    }

    public function setMostGoalsPerGame($hasMostGoalsPerGame)
    {
        $this->mostGoalsPerGame = $hasMostGoalsPerGame;
        return $this;
    }

    public function goalsPerGame()
    {
        return $this->goalsPerGame;
    }
}