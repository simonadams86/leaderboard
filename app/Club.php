<?php

namespace Leaderboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Leaderboard\Model\Game;

class Club extends Model
{

    protected $fillable = [
        'name', 'league_id', 'att', 'mid', 'def', 'ovr',
    ];

    /*protected $fillable = [
        'name', 'league_id',
    ];*/

    /*protected $with = ['league'];*/

    protected $appends = ['overall'];

    public function homeGames()
    {
        return $this->hasMany(Game::class, 'home_club_id');
    }

    public function awayGames()
    {
        return $this->hasMany(Game::class, 'away_club_id');
    }

    /*public function league()
    {
        return $this->belongsTo(League::class);
    }*/

    public function leagues()
    {
        return League::findMany($this->leaderboardGame()->pluck('league_id'));
    }

    /**
     * @return BelongsToMany
     */
    public function leaderboardGame()
    {
        return $this->belongsToMany(LeaderboardGame::class)
            ->withPivot([
                'league_id',
                'att',
                'mid',
                'def',
                'ovr',
            ]);
    }

    /**
     * @param int $leaderboardGameId
     * @return Pivot
     */
    public function getStats($leaderboardGameId)
    {
        return $this->leaderboardGame->find($leaderboardGameId)->pivot;
    }

    public function getOverallAttribute()
    {
        return $this->pivot->ovr ? : '';
    }
}
