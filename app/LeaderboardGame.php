<?php

namespace Leaderboard;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class LeaderboardGame extends Model
{

    protected $fillable = [
        'name', 'display_name',
    ];

    public function clubs()
    {
        return $this->belongsToMany(Club::class)
            ->withPivot([
                'league_id',
                'att',
                'mid',
                'def',
                'ovr',
            ]);
    }

    public function leagueClubs($leagueId)
    {
        return $this->clubs()->wherePivot('league_id', $leagueId)->get();
    }

    public function scopeGetCurrent($query)
    {
        return $query->with('clubs')->latest()->first();
    }

    /**
     * @param int $clubId
     * @return Pivot
     */
    public function getStats($clubId)
    {
        return $this->clubs->find($clubId)->pivot;
    }
}
