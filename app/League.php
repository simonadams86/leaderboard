<?php

namespace Leaderboard;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{

    protected $fillable = [
        'name',
    ];

    protected $appends = ['clubs'];

    public function getClubsAttribute()
    {
        return LeaderboardGame::with('clubs')->getCurrent()->leagueClubs($this->id);
    }

    public function clubs()
    {
        return $this->getClubsAttribute();
    }
}
