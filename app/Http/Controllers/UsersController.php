<?php

namespace Leaderboard\Http\Controllers;

use Illuminate\Http\Request;
use Leaderboard\Model\User;

class UsersController extends Controller
{

    public function index()
    {
        return User::all()->keyBy('id')->all();
    }
}
