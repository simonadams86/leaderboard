<?php

namespace Leaderboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Leaderboard\Model\LeagueTable;
use Leaderboard\Model\Statistics;
use Leaderboard\Model\User;

class TvController extends Controller
{
    public function index()
    {

        $users = User::all();

        $period = $this->getCurrentPeriod();
        $leagueTable = new LeagueTable();

        $users->each(function (User $user) use ($leagueTable, $period) {

            $leagueTable->addTeam((new Statistics($user, $period)));
        });
        $request = new Request();

        //TODO add metadata here somewhere
        $table = $leagueTable->generateTable($request)->toArray();

        return view('tv', compact('table'));
    }

    private function getCurrentPeriod()
    {
        $period = Carbon::now();
        return $period;
    }
}
