<?php

namespace Leaderboard\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Leaderboard\Model\Game;
use Leaderboard\Model\LeagueTable;
use Leaderboard\Model\Statistics;
use Leaderboard\Model\User;
use Leaderboard\Notifications\GameCreatedNotification;

class GameController extends Controller
{

    public function all(Request $request)
    {
        $users = User::all();

        $period = $this->getPeriod($request);
        $leagueTable = new LeagueTable();

        $users->each(function (User $user) use ($leagueTable, $period) {

            $leagueTable->addTeam((new Statistics($user, $period)));
        });

        //TODO add metadata here somewhere
        return $leagueTable->generateTable($request);
    }

    public function allTimeByUser(Request $request, User $user)
    {
        return new Statistics($user);
    }

    public function games()
    {
        $period = Carbon::now()->format('Y-m');

        $games = Game::with('users')
            ->take(10)
            ->where('period', 'LIKE', $period.'%')
            ->latest()
            ->get();

        return $games;
    }

    public function store(Request $request)
    {
        $game = $request->game;

        $result = "{$game['home']['score']}-{$game['away']['score']}";

        $period = Carbon::now()->toDateString();

        $newGame = Game::create([
            'result' => $result,
            'period' => $period,
            'leaderboard_game_id' => $game[ 'leaderboard_game_id' ],
            'home_club_id' => $game[ 'home_club_id' ],
            'away_club_id' => $game[ 'away_club_id' ],
        ]);

        $this->addHomeTeam($game, $newGame);
        $this->addAwayTeam($game, $newGame);

        $newGame->notify(new GameCreatedNotification());

        return response()->json(['success' => 'Kampen blev registreret', 'game' => $newGame]);
    }

    public function destroy(Game $game)
    {
        $game->delete();

        return response()->json(['success' => 'Kampen blev slettet']);
    }

    /**
     * @param $game
     * @param Game $newGame
     */
    private function addHomeTeam($game, $newGame)
    {
        foreach($game[ 'home' ][ 'users' ] as $homeUser) {
            $newGame->addUser($homeUser[ 'id' ], true);
        }
    }

    /**
     * @param $game
     * @param Game $newGame
     */
    private function addAwayTeam($game, $newGame)
    {
        foreach($game[ 'away' ][ 'users' ] as $awayUser) {
            $newGame->addUser($awayUser[ 'id' ]);
        }
    }

    /**
     * @param Request $request
     * @return null|Carbon
     */
    private function getPeriod(Request $request)
    {
        switch($request->period) {

            case 'last':
                $period = Carbon::now()->subMonthNoOverflow();
                break;

            case 'allTime':
                $period = null;
                break;

            case 'current':
            default:
                $period = Carbon::now();
                break;
        }
        return $period;
    }
}
