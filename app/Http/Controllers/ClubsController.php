<?php

namespace Leaderboard\Http\Controllers;

use Leaderboard\League;

class ClubsController extends Controller
{

    public function index()
    {
        return League::all();
    }
}
