<?php

namespace Leaderboard\Http\Middleware;

use Closure;
use Exception;
use Leaderboard\Model\DenyTheCorruptorNotifiable;
use Leaderboard\Model\User;
use Leaderboard\Notifications\DenyTheCorruptorNotification;

class DenyTheCorruptor
{

    private $user;

    private $corruptorIdentifiers = [];

    public function __construct()
    {
        /** @var User $user */
        $this->user = auth()->user();

        $this->corruptorIdentifiers[] = $this->user->nickname === 'The Corruptor';
        $this->corruptorIdentifiers[] = $this->user->email === 'christian@wecode.dk';
        $this->corruptorIdentifiers[] = str_contains(
            $this->user->name,
            ['Christian D', 'Christian Dolberg Rasmussen', 'Dolberg', 'Rasmussen']
        );
        $this->corruptorIdentifiers[] = $this->user->id === 4;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if ($this->isTheCorruptor()) {
            $this->notifyChannelOfCorruptor();
            $this->resetTheCorruptor();

            throw new Exception('Godt forsøgt The Corruptor. Du har ikke adgang til denne funktion!', 493);
        }

        return $next($request);
    }

    public function isTheCorruptor()
    {
        foreach($this->corruptorIdentifiers as $isTheCorruptor) {
            if ($isTheCorruptor) return true;
        }
        return false;
    }

    public function resetTheCorruptor()
    {
        $this->user->update([
            'name' => 'Christian Dolberg Rasmussen',
            'email' => 'christian@wecode.dk',
            'nickname' => 'The Corruptor',
        ]);
    }

    private function notifyChannelOfCorruptor()
    {
        $notifiable = new DenyTheCorruptorNotifiable();
        $notifiable->notify(new DenyTheCorruptorNotification());
    }
}
