<?php

namespace Leaderboard\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Leaderboard\Model\LeagueTable;
use Leaderboard\Model\Statistics;
use Leaderboard\Model\User;
use Leaderboard\Notifications\UserStatisticsNotification;

class SendLeaderboardStatuses extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaderboard:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends statuses to users about their current month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $period = Carbon::now();

        if ($period->isWeekend()) return false;

        $users = User::all();

        $period = Carbon::now();

        $leagueTable = new LeagueTable();

        $users->each(function (User $user) use ($leagueTable, $period) {

            $leagueTable->addTeam((new Statistics($user, $period)));
        });

        $leagueTable->setPositions()
            ->getTeams()
            ->each(function (Statistics $statistics, $key) {
                if ($statistics->user()->slack_webhook_url) {
                    $statistics->notify(new UserStatisticsNotification($statistics->getLeaguePosition()));
                }
            });
    }
}
