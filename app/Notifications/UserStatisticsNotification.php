<?php

namespace Leaderboard\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Leaderboard\Model\Statistics;
use Wecode\SmsEagle\SmsEagleChannel;
use Wecode\SmsEagle\SmsEagleMessage;

class UserStatisticsNotification extends Notification implements ShouldQueue
{

    use Queueable;

    /**
     * @var null
     */
    private $position;

    /**
     * Create a new notification instance.
     *
     * @param null|int $position
     */
    public function __construct($position = null)
    {
        //
        $this->position = $position;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack', /*SmsEagleChannel::class*/];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param Statistics $notifiable
     * @return SlackMessage
     */
    public function toSlack(Statistics $notifiable)
    {
        $message = $notifiable->dailySlackMessage($this->position);

        return (new SlackMessage)
            ->content($message[ 'title' ])
            ->attachment(function ($attachment) use ($message) {
                $attachment->title($message[ 'link_title' ], $message[ 'url' ])
                    ->content($message[ 'summary' ])
                    ->fields($message[ 'fields' ]);
            });
    }

    public function toSmsEagle(Statistics $notifiable)
    {
        return (new SmsEagleMessage(
            $notifiable->dailySmsMessage($this->position),
            $notifiable->user()->phone
        ));
    }
}
