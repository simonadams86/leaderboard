<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('/api/slack/join_game', function (\Illuminate\Http\Request $request) {

    return 'hej';

    return response()->json(['request' => $request->all(), 'user' => Leaderboard\Model\User::where('email', $request->email)->get()]);
});


Route::get('/tv', 'TvController@index')->name('home');

Route::get('/statistics/all', 'GameController@all');

Route::group(['middleware' => ['auth']], function () {


    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('statistics/{user}/all-time', 'GameController@allTimeByUser');

    Route::get('users/all', 'UsersController@index');

    Route::get('games/all', 'GameController@games');

    Route::get('clubs/all', 'ClubsController@index');

    Route::post('/statistics/store', 'GameController@store');

    Route::delete('/games/{game}', 'GameController@destroy');



    Route::group(['middleware' => ['deny_the_corruptor']], function () {

    });



    Route::get('/api/tasks', function () {

        return response()->json([['id' => 1, 'body' => 'test']]);
    });
});




