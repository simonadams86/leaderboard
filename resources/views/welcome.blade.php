@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Stilling
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <leaderboard></leaderboard>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                  {{--<img class="christmas-hat-img hidden-md-down" src="/images/Christmas-Hat.png" alt="">--}}
                    <div class="panel-heading">
                        Indtast spil
                    </div>
                    <div class="panel-body">
                        <tabs v-model="createGameActiveTab" nav-style="tabs" justified>
                            <tab header="Shuffler">
                                <shuffle-new-game></shuffle-new-game>
                            </tab>
                            <tab header="Manuel">
                                <manual-new-game></manual-new-game>
                            </tab>
                        </tabs>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Seneste kampe
                    </div>
                    <div class="panel-body">
                        <latest-games></latest-games>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <modal effect="fade/zoom" :value="showProfile" ref="modal_profile" @ok="hideProfile"
           @cancel="hideProfile">
        <slot name="modal-body" v-if="stats">
            <tabs v-model="profileActiveTab" nav-style="tabs" justified>
                <tab header="Månedens stats">
                    <ul class="list-group">
                        <li class="list-group-item">
                            Navn: @{{stats.name}}
                        </li>
                        <li class="list-group-item" v-for="(field, text) in stats.fields">
                            @{{ text }}: @{{field}}
                        </li>
                    </ul>
                </tab>
                <tab header="Seneste kampe">
                    <ul class="list-group">
                        <li class="list-group-item" v-for="game in stats.latestGamesAllTime">
                            <div class="list-group-item-text">
                                <div class="row">
                                    <div class="col-sm-3">
                                        @{{game.period_formatted}}
                                    </div>

                                    <div class="col-sm-9">
                                        @{{game.summary}}
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </tab>
                <tab header="All-time stats">
                    <stats-all-time :stats="stats"></stats-all-time>
                </tab>
            </tabs>
        </slot>
        <div slot="modal-footer"></div>
    </modal>
    <flash message="{{session('flash')}}"></flash>
@endsection
