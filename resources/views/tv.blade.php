@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Stilling
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <th>#</th>
                                    <th>Navn</th>
                                    <th>K</th>
                                    <th>V</th>
                                    <th>U</th>
                                    <th>T</th>
                                    <th>+</th>
                                    <th>-</th>
                                    <th>P</th>
                                    <th>%</th>
                                    <th>PPK</th>
                                </thead>
                                <tbody>
                                    @foreach($table['data'] as $stats => $value)
                                        <tr>
                                            <td>{{$stats+1}}</td>
                                            <td>{{$value['name']}}</td>
                                            <td>{{$value['totalGames']}}</td>
                                            <td>{{$value['wins']}}</td>
                                            <td>{{$value['draws']}}</td>
                                            <td>{{$value['losses']}}</td>
                                            <td>{{$value['goalsFor']}}</td>
                                            <td>{{$value['goalsAgainst']}}</td>
                                            <td>{{$value['points']}}</td>
                                            <td>{{$value['winPercentage']}}</td>
                                            <td>{{$value['pointsPerGame']}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
