/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.events = new Vue();

window.flash = function (message) {
    //run flash event
    window.events.$emit('flash', message);
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*Use as <tasks></tasks>*/
/*Vue.component('tasks', require('./components/test/Tasks.vue'));*/

var alert = require('vue-strap/src/alert');
var modal = require('vue-strap/src/modal');
var tabs = require('vue-strap/src/tabs');
var tab = require('vue-strap/src/tab');
var popover = require('vue-strap/src/popover');

Vue.component('flash', require('./components/Flash.vue'));
Vue.component('leaderboard', require('./components/Leaderboard.vue'));
Vue.component('manual-new-game', require('./components/ManualNewGame.vue'));
Vue.component('shuffle-new-game', require('./components/ShuffleNewGame.vue'));
Vue.component('latest-games', require('./components/LatestGames.vue'));
Vue.component('stats-all-time', require('./components/StatsAllTime.vue'));
Vue.component('award', require('./components/Award.vue'));

const app = new Vue({
    el: '#app',
    components: {
        alert: alert,
        modal: modal,
        tabs: tabs,
        tab: tab,
        popover: popover,
    },
    data: {
        showTop: true,
        showProfile: false,
        stats: null,
        profileActiveTab: 0,
        createGameActiveTab: 0,
        games: null,
    },
    events: {
        'show-profile'(stats) {
            this.stats = stats;
            this.showProfile = true;
        },
    },
    methods: {
        hideProfile: function () {
            this.showProfile = false;
            this.profileActiveTab = 0;
        },
    },
});
