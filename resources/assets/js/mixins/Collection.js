export default {

    data() {
        return {
            items: [],
        }
    },

    methods: {
        add(item) {

            this.items.push(item);

            this.$emit('added');
        },

        remove(index) {
            //grab one item and remove from collection
            this.items.splice(index, 1);

            this.$emit('removed');
        },

        shuffle(array) {
            let currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }
    }
}