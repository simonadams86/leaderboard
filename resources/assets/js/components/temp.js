(function (d, w, $, undefined) {
    "use strict";

    function log() {
        console.log.apply(console, arguments);
    }

    function err() {
        console.error.apply(console, arguments);
    }

    function has(obj, prop) {
        return Object.prototype.hasOwnProperty.call(obj, prop);
    }

    function randTime(time) {
        return Math.random() * time;
    }

    function sprintf(text) {
        var args = arguments;
        var i = 1;
        return text.replace(/%s/g, function () {
            return i < args.length ? args[i++] : "";
        });
    }

    var arrProto = Array.prototype;


    var Person = (function () {
        /*
         * @constructor Person
         *
         * @param {String} name
         *
         */

        function Person(name) {
            this.name = name;
            this.promise = null;
        }

        Person.prototype = {
            getReady: function () {
                var MAX_TIME = 1000 * 10;
                var MIN_TIME = 1000 * 4;

                var deferred = $.Deferred();

                deferred.done($.proxy(function () {
                    log(sprintf("%s is ready for the ride", this.name));
                }, this));

                setTimeout(function () {
                    deferred.resolve();
                }, Math.max(Math.random() * MAX_TIME, MIN_TIME));

                // No public facing resolove or rejecting
                this.promise = deferred.promise();

                return this;
            }
        };
        return Person;
    }());

    // Public facing Ride object
    var Ride = (function () {

        /*
         * @constructor Ride
         *
         */

        function Ride() {

            // Make this array like
            var methods = "map,slice".split(",");
            var proto = Ride.prototype;
            methods.forEach(function (method) {
                if (has(arrProto, method)) {
                    proto[method] = arrProto[method];
                }
            });

        }

        Ride.prototype = {
            length: 0,

            /*
             * @method push
             */
            push: function () {
                // Omit checking type for brevity
                // Should only allow Person objects
                var people = arrProto.slice.apply(arguments);
                var push = arrProto.push;

                // Prepare people for the ride
                people.forEach(function (person) {
                    person.getReady();
                });

                arrProto.push.apply(this, people);

                return this;
            },

            /*
             * @method prepare
             */
            prepare: function (done, fail) {
                var promises = this.map(function (person) {
                    return person.promise;
                });

                // Omit checking if person is never got ready for brevity
                log(sprintf("Preparing %s people for the ride", promises.length), promises);
                $.when.apply(null, promises).done([done, $.proxy(function () {

                    log("Everyone is Ready", arguments);
                    // Implement ride methods - could easily set up a queue
                    // for when then ride is finished and start the next set of persons
                }, this)])

                    .fail([fail, $.proxy(function () {

                        err("Fail");

                    }, this)]);

                return this;
            }

        };
        return Ride;
    }());

    var ride = new Ride;
    var joe = new Person("Joe");
    var bob = new Person("Bob");
    var john = new Person("John");

    // Get people on the ride and prepare them for fun!
    ride.push(joe, bob, john).prepare();


}(document, window, jQuery));