<?php

namespace Wecode\SmsEagle;

use GuzzleHttp\Client as GuzzleClient;

class SmsEagleClient
{

    protected $client;

    protected $url;

    protected $type;

    protected $method;

    protected $parameters;

    public function __construct()
    {
        $url = 'https://url-of-smseagle/index.php';

        /*TODO move to methods*/
        $this->type = 'jsonrpc';

        $this->method = 'sms';

        $this->url = "{$url}/{$this->type}/{$this->method}";

        $this->parameters = config('sms-eagle');

        $this->client = new GuzzleClient();
    }

    /*
     * Generic methods
     */

    public function get(SmsEagleMessage $message)
    {
        return $this->response($message);
    }

    public function post(SmsEagleMessage $message)
    {
        return $this->response($message, 'post');
    }

    public function put(SmsEagleMessage $message)
    {
        return $this->response($message, 'put');
    }

    public function delete(SmsEagleMessage $message)
    {
        return $this->response($message, 'delete');
    }

    /**
     *
     *
     * @param SmsEagleMessage $message
     * @param string $method
     * @return mixed
     */
    private function response(SmsEagleMessage $message, $method = 'get')
    {
        $response = json_decode(
            $this->client
                ->$method(
                    $this->url,
                    ['json' => [
                        'method' => 'sms.send_sms',
                        'params' => [
                            $this->parameters,
                            $message,
                        ],
                    ],
                    ])
                ->getBody()
                ->getContents()
        );

        return $response;
    }
}