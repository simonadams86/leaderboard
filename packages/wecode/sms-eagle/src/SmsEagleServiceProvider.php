<?php

namespace Wecode\SmsEagle;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SmsEagleServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        /*Copy config file to app config dir*/
        $configPath = __DIR__.'/../config/sms-eagle.php';
        $this->mergeConfigFrom($configPath, 'sms-eagle');
        $this->publishes([$configPath => config_path('sms-eagle.php')], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
