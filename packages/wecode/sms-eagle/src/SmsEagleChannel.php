<?php

namespace Wecode\SmsEagle;

use Illuminate\Notifications\Notification;

class SmsEagleChannel
{

    protected $client;

    /**
     * SmsEagleChannel constructor.
     * @param $client
     */
    public function __construct()
    {
        $this->client = new SmsEagleClient();
    }

    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        /*If needed?!*/
        $phone = $notifiable->routeNotificationForSmsEagle($notifiable);

        $message = $notification->toSmsEagle($notifiable);
        $this->client->post($message);

        // Send notification to the $notifiable instance...
    }
}