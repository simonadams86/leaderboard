<?php

namespace Wecode\SmsEagle;

class SmsEagleMessage
{

    private $content;

    private $phone;

    private $sendDate;

    private $highPriority;

    /**
     * SmsEagleMessage constructor.
     * @param $content
     * @param $phone
     * @param null $sendDate
     * @param int $highPriority
     */
    public function __construct($content, $phone, $sendDate = null, $highPriority = 0)
    {
        $this->content = $content;
        $this->phone = $phone;
        $this->sendDate = $sendDate;
        $this->highPriority = $highPriority;
    }

    public function __toString()
    {
        return "\"to\":\"{$this->phone}\",\"message\":\"{$this->content}\",\"date\":\"{$this->sendDate}\",\"highpriority\":\"{$this->highPriority}\"";
    }
}